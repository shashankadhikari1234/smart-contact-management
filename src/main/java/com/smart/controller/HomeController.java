package com.smart.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {
    @RequestMapping("/home")
    public String home(Model model){
        model.addAttribute("title","Home - Smart Contact Manger");
       return "home";
    }
    @RequestMapping("/about")
    public String about(Model model){
        model.addAttribute("title","Home - Smart Contact Manger");
        return "about";
    }
    @RequestMapping("/signup")
    public String signUp(Model model){
        model.addAttribute("title","Register - Smart Contact Manger");
        return "signup";
    }
}
