package com.smart.repo;

import com.smart.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.criteria.CriteriaBuilder;

public interface UserRepo extends JpaRepository<User,Integer> {
}
