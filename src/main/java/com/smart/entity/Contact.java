package com.smart.entity;

import lombok.*;

import javax.persistence.*;
@Entity
@Table(name = "contact")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
public class Contact {
    @Id
    @SequenceGenerator(name = "contact_gen",sequenceName = "contact_seq",allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,generator = "contact_gen")
    private Integer id;
    private String name;
    private String secondName;
    private String work;
    private String email;
    private String phone;
    private String image;
    @Column(length = 50000)
    private String description;
    @ManyToOne
    private User user;
}
